package com.horest.horest

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout

class ViewInstructions : AppCompatActivity() {
    var tabLayout: TabLayout? = null
    var next: TextView? = null
    var position = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_instructions)
        val mSlideViewPager: ViewPager = findViewById(R.id.slideViewPager)

        val usuario: String = intent.extras!!.getString("USUARIO").toString()
        tabLayout = findViewById(R.id.tabIndicator)
        next = findViewById(R.id.next)
        val sliderAdapter = SliderAdapter(this)

        position = mSlideViewPager!!.currentItem
        next?.setOnClickListener {
            if (position < 3) {
                position++
                mSlideViewPager!!.currentItem = position
            }
            if (position == 3) {
                val i = Intent(Intent(this, Inicio::class.java))
                i.putExtra("USUARIO", usuario)
                startActivity(i)
            }
        }
        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                position = tab!!.position
                if (tab.position == 2) {
                    next!!.text = "Comenzar"
                } else {
                    next!!.text = "Siguiente"
                }
            }
        })
        tabLayout?.setupWithViewPager(mSlideViewPager)
        mSlideViewPager.adapter = sliderAdapter
    }
}