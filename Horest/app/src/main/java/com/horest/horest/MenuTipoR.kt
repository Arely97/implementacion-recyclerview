package com.horest.horest

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_menu_tipo_r.*
import kotlinx.android.synthetic.main.activity_menu_tipo_r.creditos5

class MenuTipoR : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        var tipo = 0
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_tipo_r)
        val tvUsuario: TextView = findViewById(R.id.NameUs4)
        val usuario: String = intent.extras!!.getString("USUARIO").toString()
        tvUsuario.text = usuario
        uno.setOnClickListener {
            tipo = 1
            val bundle = Bundle()
            bundle.apply {
                putInt("key_tipo", tipo)
                putString("USUARIO", usuario)
            }
            val intent = Intent(this, Restaurante2::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
        dos.setOnClickListener {
            tipo = 2
            val bundle = Bundle()
            bundle.apply {
                putInt("key_tipo", tipo)
                putString("USUARIO", usuario)
            }
            val intent = Intent(this, Restaurante2::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
        tres.setOnClickListener {
            tipo = 3
            val bundle = Bundle()
            bundle.apply {
                putInt("key_tipo", tipo)
                putString("USUARIO", usuario)
            }
            val intent = Intent(this, Restaurante2::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
        cuatro.setOnClickListener {
            tipo = 4
            val bundle = Bundle()
            bundle.apply {
                putInt("key_tipo", tipo)
                putString("USUARIO", usuario)
            }
            val intent = Intent(this, Restaurante2::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
        creditos5.setOnClickListener {
            startActivity(Intent(this, Creditos::class.java))
        }
        salir4.setOnClickListener {
            startActivity(Intent(this, Login::class.java))
        }
    }
}