package com.horest.horest

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_registro.*

class Registro : AppCompatActivity() {
    var n = 100
    var i = 0
    var datos2 = arrayOfNulls<String>(n)
    var datos3 = arrayOfNulls<String>(n)
    var datos4 = arrayOfNulls<String>(n)
    var datos5 = arrayOfNulls<String>(n)
    var datos6 = arrayOfNulls<String>(n)
    var datos7 = arrayOfNulls<String>(n)
    var datos8 = arrayOfNulls<String>(n)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)
        btnGuardar.setOnClickListener {
            val nombres = edtNombre.text.toString()
            val apellidos = edtApellido.text.toString()
            var genero = if (rdbMasculino.isChecked) "Masculino" else "Femenino"
            val edad = edtEdad.text.toString()
            val us = edtUsuario.text.toString()
            val contrasena1 = edtContra1.text.toString()
            val contrasena2 = edtContra2.text.toString()

            if (nombres.isEmpty()) {
                Toast.makeText(this, "Debes ingresar todos los datos", Toast.LENGTH_SHORT).show()
            }
            if (apellidos.isEmpty()) {
                Toast.makeText(this, "Debes ingresar todos los datos", Toast.LENGTH_SHORT).show()
            }
            if (genero.isEmpty()) {
                Toast.makeText(this, "Debes ingresar todos los datos", Toast.LENGTH_SHORT).show()
            }
            if (us.isEmpty()) {
                Toast.makeText(this, "Debes ingresar todos los datos", Toast.LENGTH_SHORT).show()
            }
            if (contrasena1.isEmpty()) {
                Toast.makeText(this, "Debes ingresar todos los datos", Toast.LENGTH_SHORT).show()
            }
            if (contrasena2.isEmpty()) {
                Toast.makeText(this, "Debes ingresar todos los datos", Toast.LENGTH_SHORT).show()
            }

            if (!chkTerminos.isChecked) {
                Toast.makeText(this, "Debe aceptar los terminos", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (contrasena1 == contrasena2) {
                if (i < n) {
                    datos2[i] = nombres
                    datos3[i] = apellidos
                    datos4[i] = genero
                    datos5[i] = edad
                    datos6[i] = us
                    datos7[i] = contrasena1
                    datos8[i] = contrasena2
                }
                i++
                val bundle = Bundle()
                bundle.apply {
                    putString("key_nombres", nombres)
                    putString("key_apellido", apellidos)
                    putString("key_genero", genero)
                    putString("key_ed", edad)
                    putString("key_us", us)
                    putString("key_contra1", contrasena1)
                    putString("key_contra2", contrasena2)
                }
                Toast.makeText(this, "Registro completado", Toast.LENGTH_SHORT).show()
                val intent = Intent(this, Login::class.java).apply {
                    putExtras(bundle)
                }
                startActivity(intent)
            } else {
                Toast.makeText(this, "Las contraseñas no son iguales", Toast.LENGTH_SHORT).show()
            }
        }
        creditos.setOnClickListener {
            startActivity(Intent(this, Creditos::class.java))
        }
    }
}