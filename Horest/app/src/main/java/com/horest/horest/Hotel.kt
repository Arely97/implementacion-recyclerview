package com.horest.horest

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_hotel.*

class Hotel : AppCompatActivity() {
    var listaH: RecyclerView? = null
    var adaptador: AdaptadorCustom? = null
    var layoutManager: RecyclerView.LayoutManager? = null  //el diseño

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel)
        val tvUsuario: TextView = findViewById(R.id.NameUs2)

        val usuario: String = intent.extras!!.getString("USUARIO").toString()
        tvUsuario.text = usuario

        val bundle: Bundle? = intent.extras
        bundle?.let { bundleLibriDeNull ->
            val tipo = bundleLibriDeNull.getInt("key_tipo")
            if (tipo == 1) {
                val activity_hotel1 = ArrayList<Hotel1>()

                activity_hotel1.add(
                    Hotel1(
                        "Hotel Imperial",
                        "Calle cuña, número 03, colonia centro.",
                        3.5F,
                        R.drawable.hotel_imperial
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel Misantla",
                        "Avenida Carlos Salinas de Gortari, número 223, colonia Plan de la Vieja. Teléfono: 235-32-3-19-35.",
                        4.5F,
                        R.drawable.hotel_misantla
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel Dorado Amanecer",
                        "Boulevard Ávila Camacho, número 304 Teléfono: 235-32-3-49-02.",
                        2.5F,
                        R.drawable.hotel_dorado_amanecer
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel León",
                        "Calle Degollado esquina Francisco I. Madero, número 114, colonia Centro. Teléfono: 235-32-3-00-72.",
                        1.5F,
                        R.drawable.hotel_leon
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel Don Pablo",
                        "Avenida 5 de Mayo, número 202, colonia Centro Teléfono: 235-32-3-18-55.",
                        2.5F,
                        R.drawable.hotel_don_pablo
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel Claudia Isabel",
                        "Calle Degollado, número 116, colonia Centro. Teléfono: 235-32-3-16-05.",
                        3.0F,
                        R.drawable.hotel_claudia_isabel
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel Posada del Ángel",
                        "Calle Hidalgo, número 127, colonia Centro. Teléfono: 235-32-3-40-75.",
                        2.5F,
                        R.drawable.hotel_posada_del_angel
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel Delfos",
                        "Calle Úrsulo Galván, número 103, colonia Centro.",
                        1.5F,
                        R.drawable.hotel_delfos
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel Gilbon",
                        "Calle Degollado",
                        4.5F,
                        R.drawable.hotel_gilbon
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel Sol",
                        "Calle Morelos Col. Centro.",
                        5.0F,
                        R.drawable.hotel_sol
                    )
                )
                listaH = findViewById(R.id.ListaH)
                listaH?.setHasFixedSize(true)  //adaptador tamaño de la vista

                layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
                listaH?.layoutManager = layoutManager  // donde se dibuje el layout


                adaptador = AdaptadorCustom(this, activity_hotel1, object : ClickListener {
                    override fun onClick(vista: View, index: Int) {

                        val bundle = Bundle()
                        bundle.apply {
                            putInt("key_index", index)
                            putInt("key_tipo", tipo)
                            putString("USUARIO", usuario)
                        }
                        val intent = Intent(applicationContext, DatoH::class.java).apply {
                            putExtras(bundle)
                        }
                        startActivity(intent)
                    }
                })

                listaH?.adapter = adaptador

            }
            else if (tipo == 2) {
                val activity_hotel1 = ArrayList<Hotel1>()

                activity_hotel1.add(
                    Hotel1(
                        "Hotel Campestre Casa Blanca",
                        "Carretera Misantla- Tenochtitlán Km. 1.0 Teléfono: 235-32-3-46-14",
                        3.5F,
                        R.drawable.hotel_campestre_casa_blanca
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel, restaurant y Salon de Eventos N° 2",
                        "Vacio",
                        0.0F,
                        R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel, restaurant y Salon de Eventos N° 3",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel, restaurant y Salon de Eventos N° 4",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel, restaurant y Salon de Eventos N° 5",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel, restaurant y Salon de Eventos N° 6",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel, restaurant y Salon de Eventos N° 7",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel, restaurant y Salon de Eventos N° 8",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel, restaurant y Salon de Eventos N° 9",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel, restaurant y Salon de Eventos N° 10",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                listaH = findViewById(R.id.ListaH)
                listaH?.setHasFixedSize(true)  //adaptador tamaño de la vista

                layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

                listaH?.layoutManager = layoutManager  // donde se dibuje el layout

                adaptador = AdaptadorCustom(this, activity_hotel1, object : ClickListener {
                    override fun onClick(vista: View, index: Int) {
                        if (index == 0) {

                            val bundle = Bundle()
                            bundle.apply {
                                putInt("key_index", index)
                                putInt("key_tipo", tipo)
                                putString("USUARIO", usuario)
                            }
                            val intent = Intent(applicationContext, DatoH::class.java).apply {
                                putExtras(bundle)
                            }
                            startActivity(intent)
                        } else {
                            Toast.makeText(
                                applicationContext,
                                "Apartado no disponible",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                })

                listaH?.adapter = adaptador

            }
            else if (tipo == 3) {
                val activity_hotel1 = ArrayList<Hotel1>()

                activity_hotel1.add(
                    Hotel1(
                        "Hotel Hacienda Prom",
                        "Carretera Misantla-Xalapa Km 1.700 Teléfono: 235-32-3-09-09",
                        3.5F,
                        R.drawable.hotel_hacienda_prom
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel y Salon de Eventos N° 2",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel y Salon de Eventos N° 3",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel y Salon de Eventos N° 4",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel y Salon de Eventos N° 5",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel y Salon de Eventos N° 6",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel y Salon de Eventos N° 7",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel y Salon de Eventos N° 8",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel y Salon de Eventos N° 9",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Hotel y Salon de Eventos N° 10",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )

                listaH = findViewById(R.id.ListaH)
                listaH?.setHasFixedSize(true)  //adaptador tamaño de la vista

                layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

                listaH?.layoutManager = layoutManager  // donde se dibuje el layout

                adaptador = AdaptadorCustom(this, activity_hotel1, object : ClickListener {
                    override fun onClick(vista: View, index: Int) {
                        if (index == 0) {

                            val bundle = Bundle()
                            bundle.apply {
                                putInt("key_index", index)
                                putInt("key_tipo", tipo)
                                putString("USUARIO", usuario)
                            }
                            val intent = Intent(applicationContext, DatoH::class.java).apply {
                                putExtras(bundle)
                            }
                            startActivity(intent)
                        } else {
                            Toast.makeText(
                                applicationContext,
                                "Apartado no disponible",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                })

                listaH?.adapter = adaptador
            }
            else {
                val activity_hotel1 = ArrayList<Hotel1>()

                activity_hotel1.add(
                    Hotel1(
                        "Auto-hotel Delicias",
                        "Calle Pino Suarez",
                        3.5F,
                        R.drawable.auto_hotel_delicias
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Auto hotel Bugambilias",
                        "Carretera Misantla- Tenochtitlán Km 1.0",
                        3.5F,
                        R.drawable.auto_hotel_secretos
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Auto hotel N° 3",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Auto hotel N° 4",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Auto hotel N° 5",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Auto hotel N° 6",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Auto hotel N° 7",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Auto hotel N° 8",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Auto hotel N° 9",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                activity_hotel1.add(
                    Hotel1(
                        "Auto hotel N° 10",
                        "Vacio",
                        0.0F,
                            R.drawable.camara
                    )
                )
                listaH = findViewById(R.id.ListaH)
                listaH?.setHasFixedSize(true)  //adaptador tamaño de la vista

                layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

                listaH?.layoutManager = layoutManager  // donde se dibuje el layout

                adaptador = AdaptadorCustom(this, activity_hotel1, object : ClickListener {
                    override fun onClick(vista: View, index: Int) {
                        if (index <= 1) {

                            val bundle = Bundle()
                            bundle.apply {
                                putInt("key_index", index)
                                putInt("key_tipo", tipo)
                                putString("USUARIO", usuario)
                            }
                            val intent = Intent(applicationContext, DatoH::class.java).apply {
                                putExtras(bundle)
                            }
                            startActivity(intent)
                        } else {
                            Toast.makeText(
                                applicationContext,
                                "Apartado no disponible",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                })

                listaH?.adapter = adaptador
            }
        }
        creditos3.setOnClickListener {
            startActivity(Intent(this, Creditos::class.java))
        }
        salir2.setOnClickListener {
            startActivity(Intent(this, Login::class.java))
        }
    }
}