package com.horest.horest

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_restaurante4.*


class Restaurante4 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurante4)
        val bundle: Bundle? = intent.extras
        bundle?.let { bundleLibriDeNull ->
            val index = bundleLibriDeNull.getInt("key_index")
            val tipo = bundleLibriDeNull.getInt("key_tipo")
            val tvUsuario: TextView = findViewById(R.id.NameUs6)
            val usuario: String = intent.extras!!.getString("USUARIO").toString()
            tvUsuario.text = usuario
            if (tipo == 1) {
                val hoteles = ArrayList<Restaurante1>()
                hoteles.add(
                        Restaurante1(
                                "Restaurant Las Iguanas",
                                "Teléfono: 235-32-3-09-09.",
                                "Carretera Misantla-Xalapa Km 1.700",
                                "Comida mexicana, mariscos, aves, res y pastas.",
                                "Tiempo en funcionamiento: 3 años & Propietario: C.P.A Ma. Dolores Prom y Rodríguez",
                                R.drawable.rest_iguanas
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Restaurant Los Delfines",
                                "Teléfono: (045) 235-105-57-01.",
                                "Carretera Misantla-Tenochtitlán Km 1.0 ",
                                "Platillos de mariscos y carnes.",
                                "Tiempo en funcionamiento: 1 año & Propietario: Miguel Ángel Bandala",
                                R.drawable.rest_delfines
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Restaurant México Lindo",
                                "Teléfono: 235-32-3-49-02",
                                "Boulevard Ávila Camacho, número 203 ",
                                "Comida mexicana",
                                "Tiempo en funcionamiento: 1 año 5 meses & Propietario: Eva Guzmán Sánchez",
                                R.drawable.rest_mexicolindo
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Restaurant Los cafetos",
                                "Teléfono: 01-235-32-3-50-62.",
                                "Calle Morelos, Colonia Centro ",
                                "Comida a la carta de tipo mexicana.",
                                "Tiempo en funcionamiento: 10 años & Propietario: César Enrique Villa Uribe",
                                R.drawable.rest_cafetos
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Restaurant Totonacapan",
                                "Teléfono: 01-235-32-3-10-28",
                                "Calle Degollado, número 116, Colonia Centro ",
                                "Comida típica de la región.",
                                "Tiempo en funcionamiento: 25 años & Propietario: Rita Álvarez Aguilar",
                                R.drawable.rest_totonacapan
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Restaurant-Bar La Herradura",
                                "Teléfono: 235-32-3-05-42.",
                                "Avenida Morelos No. 106 Letra A, colonia Centro ",
                                "Comida típica de Misantla con especialidad en empapatadas y dobladas misantecas..",
                                "Tiempo en funcionamiento: 1 año y medio & Propietario: Mario Romero y Carrera",
                                R.drawable.rest_herradura
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Restaurant-Bar Vista Hermosa",
                                "Teléfono: (045) 235-107-43-45.",
                                "Carretera Misantla-Xalapa Km 2 ",
                                "Comida típica de Misantla con especialidad en empapatadas y dobladas misantecas.",
                                "Tiempo en funcionamiento: 10 años & Propietario: Mario Romero y Carrera",
                                R.drawable.rest_vista_hermosa
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Restaurant-Bar La Ostra del Golfo",
                                "Teléfono: 01-235-32-3-00-60",
                                "Calle Aquiles Serdán, s/n, Colonia Centro ",
                                "Mariscos, Carnes y comida mexicana.",
                                "Tiempo en funcionamiento: 20 años & Propietario: Eric Lara Meza",
                                R.drawable.rest_ostra_del_golfo
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Restaurant-Bar El Quelite",
                                "Teléfono: 01-235-3-14-33",
                                "Calle Alfonso Arroyo Flores, s/n, Colonia Centro ",
                                "Mariscos.",
                                "Propietario: Julio Dante Romero.",
                                R.drawable.rest_quelite_2
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Restaurant Tiburones Rojos",
                                "Teléfono: (045) 235-101-58-68.",
                                "Avenida Ávila Camacho, s/n, Colonia Centro ",
                                "Platillos y cocteles de mariscos.",
                                "Tiempo en funcionamiento: 3 años y medio & Propietario:Oscar Bandala Rodríguez ",
                                R.drawable.rest_tiburones_rojos
                        )
                )
                NombreHotelres.setText(hoteles.get(index).Nombreres)
                DireTelefonores.setText(hoteles.get(index).DireTelefonores)
                Serviciores.setText(hoteles.get(index).Serviciores)
                AntiProres.setText(hoteles.get(index).AntiProres)
                Habitacionres.setText(hoteles.get(index).Habitacionres)
                imageHotelres.setImageResource(hoteles.get(index).fotores)
            }
            else if (tipo == 2) {
                val hoteles = ArrayList<Restaurante1>()
                hoteles.add(
                        Restaurante1(
                                "Cocina Económica  La Fondita",
                                "Teléfono: 235-32-3-50-62",
                                "Calle Bojalil Gil, número 112, Colonia Centro ",
                                "Platillos de mariscos y carnes.",
                                "Tiempo en funcionamiento: 1 año & Propietario: Miguel Ángel Bandala",
                                R.drawable.rest_lafondita
                        )
                )
                        hoteles.add(
                        Restaurante1(
                                "Cocina Económica La Sopa y +",
                                "Teléfono: 01-235-32-3-50-23",
                                "Calle Comomfort esquina Mina, número 123, Colonia Centro",
                                "Comida corrida, mariscos y carnes.",
                                "Tiempo en funcionamiento: 7 años & Propietario: U.C. María Asunción López Barrios",
                                R.drawable.rest_lasopa
                        )
                )
                       hoteles.add(
                        Restaurante1(
                                "Venezia Pizza Pasta y Vino",
                                "Teléfono: 01-235-32-3-50-62",
                                "Calle Bojalil Gil, número 112, Colonia Centro",
                                "Comida italiana.",
                                "Tiempo en funcionamiento:  1 año y medio & Propietario:Julio Dante Romero",
                                R.drawable.rest_venecio
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Pizzas Angelotti",
                                "Teléfono: (045) 235-104-94-74",
                                "Calle Camilo González, número 108, Colonia Centro",
                                "Pizzas, espagueti, papas al horno y botanas.",
                                "Tiempo en funcionamiento: 10 años & Propietario: Luis Guillermo Guillomen Díaz.",
                                R.drawable.rest_angelotti
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Pizzería Stefani",
                                "Teléfono: (045) 32-3-22-05",
                                "Calle Comonfort Número 121, Colonia Centro",
                                "Pizzas",
                                "Tiempo en funcionamiento: 10 años",
                                R.drawable.rest_pizzer_a_stefani
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Pollos Dany",
                                "Teléfono: 01-235-32-3-19-32",
                                "Calle Francisco I. Madero esquina Bojalil Gil, número 200, Colonia Centro Sucursal Boulevard Ávila Camacho, número 712.",
                                "Pollo rostizado y asado, en la sucursal también ofrece comida buffet.",
                                "Tiempo en funcionamiento: 20 años y en sucursal 4 años & Propietario:Aurora Zamora Francisco",
                                R.drawable.rest_pollodany
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Refresquería El Kiosco",
                                "",
                                "Parque Morelos, Col. Centro",
                                "Refrescos, café, tortas, sándwich, preparados.",
                                "Tiempo en funcionamiento: 25 años. & Propietario: Raúl Alfredo Rodríguez y Terán.",
                                R.drawable.rest_el_kiosco
                        )
                        )

                NombreHotelres.setText(hoteles.get(index).Nombreres)
                DireTelefonores.setText(hoteles.get(index).DireTelefonores)
                Serviciores.setText(hoteles.get(index).Serviciores)
                AntiProres.setText(hoteles.get(index).AntiProres)
                Habitacionres.setText(hoteles.get(index).Habitacionres)
                imageHotelres.setImageResource(hoteles.get(index).fotores)
            }
            else if (tipo == 3) {
                val hoteles = ArrayList<Restaurante1>()
                hoteles.add(
                        Restaurante1(
                                "Restaurant Familiar  El Quelite Fiesta",
                                "50 personas en restaurante Salón de eventos para 100 personas",
                                "Avenida 5 de Mayo, número 204, Colonia Centro Teléfono: 01-235-32-3-04-13",
                                "Comida tradicional, carnes Especialidad en Mariscos, desayunos completos, comida y cena. 7:30 am -11 pm",
                                "Tiempo en funcionamiento: 25 años & Propietario: Julio Dante Romero",
                                R.drawable.rest_quelite_2
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Hotel Campestre Casa Blanca",
                                "Restaurant y salón social para 300 personas",
                                "Carretera Misantla- Tenochtitlán Km. 1.0 Teléfono: 235-32-3-46-14",
                                "Agua caliente y fría, televisión por cable, internet, aire acondicionado, servicio de restaurant, albercas.",
                                "Tiempo en funcionamiento: 3 años & Propietario: Lic. Rafael Salazar García",
                                R.drawable.rest_casa_blanca
                        )
                )

                NombreHotelres.setText(hoteles.get(index).Nombreres)
                DireTelefonores.setText(hoteles.get(index).DireTelefonores)
                Serviciores.setText(hoteles.get(index).Serviciores)
                AntiProres.setText(hoteles.get(index).AntiProres)
                Habitacionres.setText(hoteles.get(index).Habitacionres)
                imageHotelres.setImageResource(hoteles.get(index).fotores)
            }
            else if (tipo == 4) {
                val hoteles = ArrayList<Restaurante1>()
                hoteles.add(
                        Restaurante1(
                                "Coctelería La Ostra",
                                "Teléfono: 01-235-32-3-00-60",
                                "Calle Zaragoza, número 104, Colonia Centro",
                                "Teléfono: 01-235-32-3-00-60",
                                "Tiempo en funcionamiento: 22 años & Propietario: Eric Lara Meza",
                                R.drawable.rest_ostra_del_golfo
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Coctelería El Calamar",
                                "Teléfono: (045) 235-106-26-06",
                                "Calle Zaragoza, número 103, Colonia Centro",
                                "Cocteles de mariscos.",
                                "Tiempo en funcionamiento: 16 años & Propietario: Julio César Prom Gil.",
                                R.drawable.rest_cocteler_a_el_calamar
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Coctelería Tiburones Rojos",
                                "Teléfono: (045) 235-101-58-68",
                                "Avenida Ávila Camacho, s/n, Colonia Centro",
                                "Platillos y cocteles de mariscos.",
                                "Tiempo en funcionamiento: 3 años y medio & Propietario: Oscar Bandala Rodríguez",
                                R.drawable.rest_tiburones_rojos
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Buffet El Taco Mexicano",
                                "Teléfono: (045) 235-104-81-55",
                                "Colonia Teresita Peñafiel, s/n",
                                "Comida mexicana, carne de res, borrego y cerdo, servicio buffet y platillos a la carta.",
                                "Tiempo en funcionamiento: 4 años & Propietario: Oliverio García Miranda",
                                R.drawable.rest_tacomexicano
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Las Gordas Antojitos",
                                "Teléfono: (045) 235-106-41-64",
                                "Calle Juárez esquina Salvador Díaz Mirón, s/n, Colonia Centro y sucursal en calle Alfonso Arroyo Flores esquina 5 Mayo. ",
                                "Antojitos mexicanos.",
                                "Tiempo en funcionamiento: 6 y 4 años en sucursal. & Propietario: Eréndira Carreón Córdova",
                                R.drawable.rest_gordas
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Antojitos Tere",
                                "Desconocido",
                                "Esquina Bojalil Gil",
                                "Antojitos y platillos mexicanos.",
                                "Desconocido",
                                R.drawable.rest_antojitos_tere
                        )
                        )
                hoteles.add(
                        Restaurante1(
                                "Cenaduría Lita",
                                "Desconocido",
                                "Calle Lerdo, número 125, Colonia Centro",
                                "Antojitos mexicanos",
                                "Tiempo en funcionamiento:30 años & Propietario: Ángela Tejeda Natividad.",
                                R.drawable.rest_cenadur_a_lita
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Cenaduría Magui",
                                "Teléfono: (045) 235-101-34-19",
                                "Calle Hidalgo, s/n, Colonia Centro",
                                "Antojitos mexicanos.",
                                "Tiempo en funcionamiento: 50 años & Propietario: Rosa Tejeda Natividad.",
                                R.drawable.rest_cenadur_a_magui
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Asadero",
                                "Desconocida",
                                "Linda Vista Carretera Misantla-Martinez de la Torre",
                                "Comida tìpica Misanteca",
                                "Tiempo en funcionamiento: & Propietario",
                                R.drawable.rest_asadero_linda_vista
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Taquería La Chiquitita",
                                "Desconocido",
                                "Boulevard Ávila Camacho con calle Rosendo Álvarez, s/n, Colonia Centro",
                                "Tacos de cerdo y pollo.",
                                "Tiempo en funcionamiento: 24 años& Propietario: Catalina Colorado Méndez.",
                                R.drawable.rest_taqueirabj
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Taquería BJ",
                                "Teléfono: 01-235-32-3-47-67",
                                "Calle Nizin, número 164, Colonia Benito Juárez.",
                                "Tacos de cerdo.",
                                "Tiempo en funcionamiento:30 años & Propietario: Victor Luna Grajales",
                                R.drawable.rest_taqueirabj
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Taquería Raquel",
                                "Teléfono: 01-235-32-3-08-04",
                                "Calle Zaragoza, número 102, Colonia Centro",
                                "Tacos de cerdo y pollo.",
                                "Tiempo en funcionamiento: 10 años & Propietario: Raquel Castellanos Guzmán.",
                                R.drawable.rest_raquel
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Taquería El Negro ",
                                "Teléfono: (045) 235-105-27-69",
                                "Boulevard Ávila Camacho esquina Dante Delgado, s/n.",
                                "Tacos de cerdo y res.",
                                "Tiempo en funcionamiento: 8años & Propietario: Darío Báez Sánchez.",
                                R.drawable.rest_taqueiranegro
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Taquería Lulú",
                                "Teléfono: 01-235-32-3-10-80",
                                "Calle Xalapa, número 117, Colonia Centro",
                                "Tacos de cerdo.",
                                "Tiempo en funcionamiento: 28 años & Propietario:Ma. De Lourdes Toribio y Gómez.",
                                R.drawable.rest_taquer_a_lul_
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Taquería El Güero",
                                "Desconocido",
                                "Calle Justa García Katz, Colonia Benito Juárez",
                                "Tacos de cerdo y pollo.",
                                "Tiempo en funcionamiento: 50 años & Propietario: Dulce María Gómez Alvarado.",
                                R.drawable.rest_taquer_a_el_g_ero
                        )
                )
                hoteles.add(
                        Restaurante1(
                                "Taquería Vielo´s ",
                                "Teléfono: 01-235-105-23-92",
                                "Boulevard Ávila Camacho, número 434, Colonia Centro",
                                "Tacos de cerdo, pollo, suadero y tripa de res.",
                                "Tiempo en funcionamiento: 22 años & Propietario:Gabriel Villa Sánchez.",
                                R.drawable.rest_taquer_a_vielo_s
                        )
                )
                NombreHotelres.setText(hoteles.get(index).Nombreres)
                DireTelefonores.setText(hoteles.get(index).DireTelefonores)
                Serviciores.setText(hoteles.get(index).Serviciores)
                AntiProres.setText(hoteles.get(index).AntiProres)
                Habitacionres.setText(hoteles.get(index).Habitacionres)
                imageHotelres.setImageResource(hoteles.get(index).fotores)
            }

        }
        salir6.setOnClickListener {
            startActivity(Intent(this, Login::class.java))
        }
        creditos7.setOnClickListener {
            startActivity(Intent(this, Creditos::class.java))
        }
    }
}

